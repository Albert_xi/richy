from braces.views import FormMessagesMixin, LoginRequiredMixin
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views.generic import CreateView, RedirectView, TemplateView, UpdateView

from ..core.models import Item
from ..core.views import UserItemManipulationMixin
from .forms import StakingForm
from .models import Attachment, Staking


class IndexRedirectView(LoginRequiredMixin, RedirectView):
    """
    Redirects use to open transactions page.
    """

    pattern_name = "staking:open"


class BaseStakingManipulationMixin(
    LoginRequiredMixin, UserItemManipulationMixin, FormMessagesMixin
):
    model = Staking
    form_class = StakingForm
    form_valid_message = _("Staking has been saved.")
    form_invalid_message = _("Staking hasn't been saved. Please check the form.")

    def get_success_url(self):
        """
        Redirects user back to appropriate transaction list.
        """

        if self.object.is_sold:
            return reverse("staking:closed")

        return reverse("staking:open")

    def form_valid(self, form):
        form.instance.user = self.request.user

        return super().form_valid(form)


class StakingListMixin:
    def get_stakings(self):
        return (
            Staking.objects.select_related("item", "transaction")
            .by_user(self.request.user)
            .order_by("-start", "-pk")
        )


class OpenCreateView(BaseStakingManipulationMixin, StakingListMixin, CreateView):
    """
    Handles open transaction list and it's stats.
    """

    template_name = "staking/open.pug"

    def get_initial(self):
        initial = super().get_initial()
        self.has_new_item = False

        if item := self.request.GET.get("new"):
            initial["item"] = Item.objects.get(pk=item)
            self.has_new_item = True

        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["has_new_item"] = self.has_new_item

        # Stakings list.
        context["stakings"] = self.get_stakings().open().filter(is_sold=False)

        return context


class ClosedTemplateView(LoginRequiredMixin, StakingListMixin, TemplateView):
    template_name = "staking/closed.pug"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Stakings list.
        context["stakings"] = self.get_stakings()

        return context


class DeleteStakingRedirectView(BaseStakingManipulationMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        staking = get_object_or_404(
            Staking, pk=self.kwargs["pk"], user=self.request.user
        )

        if staking.is_sold:
            to_return = reverse("staking:closed")

        to_return = reverse("staking:open")

        staking.delete()
        messages.success(self.request, _("Staking has been deleted."))

        return to_return


class StakingUpdateView(BaseStakingManipulationMixin, UpdateView):
    template_name = "staking/update.pug"

    def get_initial(self):
        initial = super().get_initial()

        if self.object.attachment_set.count():
            initial["attachments"] = list(self.object.attachment_set.all())

        return initial


class DeleteAttachmentRedirectView(LoginRequiredMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        attachment = get_object_or_404(
            Attachment, pk=kwargs["pk"], staking__user=self.request.user
        )
        attachment.delete()

        messages.success(self.request, _("Attachment has been deleted."))

        return self.request.META["HTTP_REFERER"]
