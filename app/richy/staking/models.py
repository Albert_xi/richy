import shutil
from datetime import date
from pathlib import Path

from django.conf import settings
from django.db import models
from django.template.defaultfilters import date as date_filter

from ..core.models import (
    Item,
    UserRelatedManager,
    UserRelatedModel,
    UserRelatedQuerySet,
)
from ..transactions.models import AttachmentMixin, Exchange, Transaction


def get_staking_attachment_dir(instance, filename):
    return Path("staking") / str(instance.staking.pk) / filename


class StakingQuerySet(UserRelatedQuerySet):
    def open(self):
        return self.filter(end__gte=date.today())

    def closed(self):
        return self.filter(end__lt=date.today())

    def sold(self):
        return self.filter(is_sold=True)

    def not_sold(self):
        return self.filter(is_sold=False)


class StakingManager(UserRelatedManager):
    def get_queryset(self):
        return StakingQuerySet(self.model, using=self._db)

    def open(self):
        return self.get_queryset().filter(end__gte=date.today())

    def closed(self):
        return self.get_queryset().filter(end__lt=date.today())

    def sold(self):
        return self.get_queryset().filter(is_sold=True)

    def not_sold(self):
        return self.get_queryset().filter(is_sold=False)


class Staking(UserRelatedModel):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    exchange = models.ForeignKey(Exchange, on_delete=models.PROTECT)
    start = models.DateField()
    end = models.DateField()
    amount = models.FloatField()
    reward_amount = models.FloatField()
    # TODO:  Should be mandatory once gifts are introduced.
    transaction = models.ForeignKey(
        Transaction, on_delete=models.CASCADE, blank=True, null=True
    )
    note = models.TextField(blank=True, null=True)
    is_sold = models.BooleanField(default=False)

    objects = StakingManager()

    def __str__(self):
        return f"{date_filter(self.start)} - {date_filter(self.end)}, {self.amount} {self.item.symbol} ({self.exchange})"

    def delete(self, *args, **kwargs):
        """
        Removes whole attachments dir.
        """

        shutil.rmtree(
            Path(settings.MEDIA_ROOT) / "staking" / str(self.pk),
            ignore_errors=True,
        )

        return super().delete(*args, **kwargs)


class Attachment(models.Model, AttachmentMixin):
    staking = models.ForeignKey(Staking, on_delete=models.CASCADE)
    file = models.FileField(upload_to=get_staking_attachment_dir)

    def __str__(self):
        return self.file.name
