from datetime import date

from django import forms
from django.core.validators import MinValueValidator
from django.utils.translation import gettext as _

from ..core.fields import MultipleFileField
from ..core.models import Item
from ..transactions.models import Transaction
from .models import Attachment, Staking


class StakingForm(forms.ModelForm):
    attachments = MultipleFileField(
        required=False, delete_url_pattern="staking:delete_attachment"
    )

    class Meta:
        model = Staking
        fields = (
            "item",
            "exchange",
            "start",
            "end",
            "amount",
            "reward_amount",
            "transaction",
            "note",
            "is_sold",
        )

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["item"].queryset = (
            user.get_items().filter(coin__isnull=False).order_by("symbol")
        )
        self.fields["transaction"].queryset = (
            Transaction.objects.by_user(user)
            .filter(is_closed=False, item__coin__isnull=False)
            .order_by("-date")
        )
        self.fields["amount"].validators = [MinValueValidator(0)]
        self.fields["reward_amount"].validators = [MinValueValidator(0)]

    def clean(self):
        data = super().clean()
        start = data.get("start")

        # Check start and end dates.
        if start > date.today():
            self.add_error("start", _("Date cannot be greater than today."))

        if start >= data.get("end"):
            self.add_error(
                "start", _("Date cannot be greater (or equal) than end date.")
            )

        trans = data.get("transaction")

        if trans and trans.date > start:
            self.add_error(
                "start",
                _("Date cannot be less than a date of the related transaction."),
            )

        return data

    def save(self, *args, **kwargs):
        to_return = super().save(*args, **kwargs)

        for file in self.files.getlist("attachments"):
            Attachment.objects.create(
                staking=self.instance,
                file=file,
            )

        return to_return
