import os

from celery import Celery
from django.conf import settings

# Set the default Django settings module for the 'celery' program.
if "true" == os.environ.get("DJANGO_DEBUG"):
    settings_module = "richy.settings.dev"
else:
    settings_module = "richy.settings.base"

os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_module)

app = Celery("richy")

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
