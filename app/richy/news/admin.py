from django.contrib import admin

from .models import News


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    list_display = ("title", "url", "get_item_symbol", "is_video", "date")

    def get_queryset(self, request):
        q = super().get_queryset(request)

        return q.select_related("item")

    def get_item_symbol(self, obj):
        return obj.item.symbol

    get_item_symbol.short_description = "Symbol"
