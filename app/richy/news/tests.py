import logging

from ..coins.models import Coin
from ..core.tests import BaseTestCase, create_items
from ..shares.models import Share
from .models import News
from .tasks import download_coin_news, download_share_news

logger = logging.getLogger(__name__)


class NewsTestCase(BaseTestCase):
    def test_download_share_news(self):
        # Let's operate with AAPL ticker which should
        # have more than 5 news.
        c = Share(symbol="AAPL")
        c.save()

        download_share_news(c)

        # Let's consider more than 5 results as success.
        self.assertTrue(5 < News.objects.count())

    def test_download_coin_news(self):
        create_items()
        # Let's operate with BTC ticker which should
        # have more than 5 news.
        c = Coin.objects.get(symbol="BTC")

        download_coin_news(c)

        # Let's consider more than 5 results as success.
        self.assertTrue(5 < News.objects.count())
