from django import forms
from django.utils.translation import gettext as _
from karpet import Karpet

from ..core.forms import BaseUserItemForm
from .models import Coin


class UserCoinForm(BaseUserItemForm):
    item_model = Coin
    coin_id = forms.CharField(
        label=_("Coin ID"), help_text=_("GeckoCoin.com identifier.")
    )

    def __init__(self, user, *args, **kwargs):
        super().__init__(user, *args, **kwargs)

        if self.instance.pk:
            self.fields["symbol"].widget.attrs["readonly"] = True
            self.fields["symbol"].initial = self.instance.item.symbol
            self.fields["coin_id"].widget.attrs["readonly"] = True
            self.fields["coin_id"].initial = self.instance.item.coin.coin_id

    def clean(self):
        cleaned_data = super().clean()
        symbol = cleaned_data.get("symbol")
        coin_id = cleaned_data.get("coin_id")

        # User didn't specified coin ID. We try to look for
        # the ID based on symbol. If we find more than 1 ID
        # we raise an error - user needs to specify ID.
        if not symbol:
            return cleaned_data

        coin_ids = Karpet().get_coin_slugs(symbol)

        if not coin_id and 1 < len(coin_ids):
            self.add_error(
                "coin_id", _("Coin has ambiguous symbol. Please enter coin ID.")
            )

        # 1. Did we found anything?
        # 2. Does the entered coin ID equals what we found?
        if not coin_ids or (coin_id and coin_id not in coin_ids):
            self.add_error("coin_id", "Coin ID wasn't found.")

        return cleaned_data
