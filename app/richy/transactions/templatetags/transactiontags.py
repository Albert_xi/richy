from django import template

register = template.Library()


@register.simple_tag
def average_price(transactions):
    return sum([t.get_value() for t in transactions]) / sum(
        [t.amount for t in transactions]
    )
