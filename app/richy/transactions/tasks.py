import logging
from time import sleep

from ..celery import app
from ..core.models import Asset
from ..shares.models import Share
from .dividends import DividendTransactions
from .models import Transaction
from .transactions import Graph

LOGGER = logging.getLogger(__name__)


@app.task(queue="fast")
def generate_graphs():
    """
    Generates transaction graphs for all transactions.
    """

    for trans in Transaction.objects.exclude(item__coin__isnull=True):
        LOGGER.debug(f"Generating transaction graph for transaction {trans.pk}")

        try:
            graph = Graph(trans)
            graph.generate()
        except:
            LOGGER.exception(
                "Couldn't generate transaction graph", extra={"transation": trans.pk}
            )
            LOGGER.debug(f"Transaction: {trans.pk} ^")
        else:
            if graph.nodes:
                Asset.upload(
                    graph.export(),
                    Asset.TRANSACTION_GRAPH,
                    transaction=trans,
                    extension="png",
                )

            else:
                LOGGER.debug(
                    f"Transaction graph for transaction {trans.pk} has no nodes."
                )


@app.task
def calculate_dividends(share=None):
    """
    Calculates dividends based on transaction (Transaction)
    and dividends (Dividend).

    :param int share: Share PK.
    """

    if share:
        DividendTransactions(Share.objects.get(pk=share)).calculate()
    else:
        for share in Share.objects.all():
            try:
                DividendTransactions(share).calculate()
            except:
                LOGGER.exception(
                    "Couldn't calculate share dividends", extra={"share": share}
                )
                LOGGER.debug(f"Share: {share.symbol} ^")

            sleep(5)
