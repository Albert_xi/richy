from django.contrib import admin

from .models import Exchange, Transaction


@admin.register(Exchange)
class ExchangeAdmin(admin.ModelAdmin):
    list_display = ["title"]


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = [
        "date",
        "item",
        "exchange",
        "amount",
        "fee",
        "is_deposit",
        "is_closing",
        "is_closed",
    ]
    ordering = ["-date"]
