import logging
from unittest.mock import patch

from django.urls import reverse

from ..core.models import Price
from ..core.scraper import CurrentPrice
from ..core.tests import (
    BaseCacheTestCase,
    BaseDecimalFormatTestCase,
    BaseDeleteUserItemTestCase,
    BaseTestCase,
    create_items,
)
from .models import Index
from .tasks import fetch_current_price, fetch_historical_data

LOGGER = logging.getLogger(__name__)


class HistoricalDataTestCase(BaseTestCase):
    def test_fetch_historical_data(self):
        """
        Tests fetching historical data (prices) of an index.
        """

        # Let's operate with SOX ticket which should
        # have more than 1000 historical records.
        i = Index(symbol="SOX")
        i.save()

        fetch_historical_data(i.pk)

        count = Price.objects.count()
        LOGGER.debug(f"Downloaded {count} historical records.")

        # Let's consider more than 1000 results as success.
        self.assertTrue(1000 < count)


class CurrentPriceTestCase(BaseCacheTestCase):
    @patch("rug.yahoo.Yahoo.get_current_price_change")
    def test_index(self, mock):
        """
        Tests index current price, market state,  price change in
        percents and value caching.
        """

        # SOX data for 21.11.2022
        mock.return_value = {
            "state": "open",
            "pre_market": {"change": {"percents": 0.0, "value": 0.0}, "value": 0.0},
            "current_market": {
                "change": {"percents": -1.9325081000000002, "value": -52.64209},
                "value": 2671.3872,
            },
            "post_market": {"change": {"percents": 0.0, "value": 0.0}, "value": 0.0},
        }

        create_items()

        index = Index.objects.get(symbol="SOX")
        fetch_current_price(index.pk)
        price = index.get_last_price(current=True)

        self.assertEqual(price.price, mock.return_value["current_market"]["value"])
        self.assertEqual(price.state, "open")
        self.assertEqual(
            price.change_value, mock.return_value["current_market"]["change"]["value"]
        )
        self.assertEqual(
            price.change_percents,
            mock.return_value["current_market"]["change"]["percents"],
        )


class DeleteTestCase(BaseDeleteUserItemTestCase):
    model = Index
    url = "indexes:delete_index"
    target_url = "indexes:overview"
    symbol = "SOX"
    task = fetch_historical_data

    def test(self):
        self.spawn()


class IndexDecimalFormatTestCase(BaseDecimalFormatTestCase):
    def test_price(self):
        self.perform_test_price_format(reverse("indexes:overview"), True)

    def test_overview_chart_price(self):
        self.perform_test_overview_chart_price_format(reverse("indexes:overview_fetch"))

    def test_item_detail_current_price(self):
        for user_item in self.user.useritem_set.filter(item__index__isnull=False):
            user_item.item.set_current_price_and_change(
                CurrentPrice(price=self.generate_price())
            )
            self.perform_test_item_detail_current_price_format(
                user_item.item.symbol,
                reverse("indexes:index_detail", args=[user_item.pk]),
            )

    def test_item_detail_yoy_change(self):
        for user_item in self.user.useritem_set.filter(item__index__isnull=False):
            self.perform_test_item_detail_yoy_change(
                user_item.item,
                reverse("indexes:index_detail", args=[user_item.pk]),
            )

    def test_item_detail_ath(self):
        for user_item in self.user.useritem_set.filter(item__index__isnull=False):
            self.perform_test_item_detail_ath(
                user_item.item, reverse("indexes:index_detail", args=[user_item.pk])
            )

    def test_item_detail_year_low_high(self):
        for user_item in self.user.useritem_set.filter(item__index__isnull=False):
            self.perform_test_item_detail_year_low_high(
                user_item.item, reverse("indexes:index_detail", args=[user_item.pk]), 0
            )

    def test_item_detail_chart_data(self):
        for user_item in self.user.useritem_set.filter(item__index__isnull=False):
            self.perform_test_item_detail_chart_price_format(
                user_item.item.symbol,
                reverse("indexes:index_fetch", args=[user_item.pk]),
            )

    def test_item_performance_chart_data(self):
        for user_item in self.user.useritem_set.filter(item__index__isnull=False):
            self.perform_test_item_performance_chart_price_format(
                user_item.item.symbol,
                reverse("indexes:performance", args=[user_item.pk]),
            )

    def test_item_performance_table_data(self):
        for user_item in self.user.useritem_set.filter(item__index__isnull=False):
            self.perform_test_item_performance_table_price_format(
                user_item.item.symbol,
                reverse("indexes:performance", args=[user_item.pk]),
            )
