from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _

from ..indexes.models import Index
from ..transactions.models import Transaction
from .models import UserItem


###############
#   Widgets   #
###############
class NoLabelCheckboxSelectMultiple(forms.CheckboxSelectMultiple):
    """
    In hiearchy of templates skips input_option.html widget
    template which handles label rendering.
    """

    option_template_name = "django/forms/widgets/input.html"


#############
#   Forms   #
#############
class SeachForm(forms.Form):
    query = forms.CharField(label=_("Search"))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["query"].widget.attrs["autocomplete"] = "off"


class BaseUserItemForm(forms.ModelForm):
    # Item part
    symbol = forms.CharField(label=_("Symbol"))
    # Coin part

    class Meta:
        model = UserItem
        exclude = ["user", "item"]

    # def __init__(self, *args, **kwargs):
    #
    #     kwargs.setdefault("label_suffix", "")
    #     super().__init__(*args, **kwargs)

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user
        self.fields["indexes"].queryset = Index.objects.by_user(user)
        self.fields["note"].help_text = _(
            "You can use #hashtags which will be recognized on Dashboard."
        )
        self.fields["symbol"].help_text = _(
            'Symbols based on yahoo.finance.com. For indexes don\'t use the "^".'
        )

        if self.instance.pk:
            self.fields["symbol"].widget.attrs["readonly"] = True
            self.fields["symbol"].initial = self.instance.item.symbol

    def clean_symbol(self):
        """
        Converts symbol to UPPERCASE.

        :return: Uppercased symbol.
        :rtype: str
        """

        symbol = self.cleaned_data.get("symbol")

        # For new items we do check duplicity.
        if (
            not self.instance.pk
            and self.item_model.objects.filter(symbol=symbol).exists()
            and self.user.useritem_set.filter(
                item=self.item_model.objects.get(symbol=symbol)
            ).exists()
        ):
            raise forms.ValidationError(_("This item has been already added."))

        return symbol.upper()

    def clean_is_archived(self):
        """
        Checks if item can be archived or not.

        :raise ValidationError: In case when item cannot be archived.
        :return: Archived flag value.
        :rtype: bool
        """

        is_archived = self.cleaned_data.get("is_archived")

        if (
            is_archived
            and self.instance.pk
            and Transaction.objects.by_user(self.user)
            .filter(item=self.instance.item, is_closed=False)
            .exists()
        ):
            raise ValidationError(
                _("Item has open transaction thus cannot be archived.")
            )

        return is_archived
