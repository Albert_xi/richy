class Ajax {

    /**
     * Performs GET request to server.
     * 
     * @param {String} url URL to be requested.
     * @param {Object} params Object with all (GET/POST) params.
     * @param {Object} options Options to be passed into fetch() method.
     * @returns {Promise} Promise returned by fetch() method.
     */
    static _fetch(url, params, options) {

        // Needed for cookies.
        options["credentials"] = "same-origin"

        // Transforms params for GET method into URL params.
        if (!options.method && params)
            url += "?" + Object.keys(params).map(function(k) {

                if (params[k] != undefined)
                    return encodeURIComponent(k) + '=' + encodeURIComponent(params[k])
                
            }).join("&")
        
        // Serializes params for POST method into JSON.
        else
            options["body"] = JSON.stringify(params)

        return fetch(url, options).then(response => {
            return response.json()
        })
    }

    /**
     * Performs GET request to server.
     * 
     * @param {String} url URL to be requested.
     * @param {Object} params Object with all GET params.
     * @returns {Promise} Promise returned by fetch() method.
     */
    static get(url, params) {

        return Ajax._fetch(url, params, {
            headers: {
                "X-Requested-With": "XMLHttpRequest",
            }
        })
    }

    /**
     * Performs POST request to server.
     * 
     * @param {String} url URL to be requested.
     * @param {Object} params Object with all POST params.
     * @returns {Promise} Promise returned by fetch() method.
     */
    static post(url, params) {

        return Ajax._fetch(url, params, {
            method: "post",
            headers: {
                "X-Requested-With": "XMLHttpRequest",
            }
        })
    }
}


export default Ajax
