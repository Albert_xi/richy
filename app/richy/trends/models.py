from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.translation import gettext as _

from ..core.models import Item, UserRelatedModel


class Trend(UserRelatedModel):
    GOOGLE = 1
    REDDIT = 2

    NETWORK_CHOICES = (
        (GOOGLE, _("Google")),
        (REDDIT, _("Reddit")),
    )

    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    network = models.PositiveSmallIntegerField(choices=NETWORK_CHOICES)
    keywords = ArrayField(models.CharField(max_length=100))
    since = models.DateField()
    data = models.JSONField(blank=True, null=True)

    # TODO: unikatni klic na kombinaci itemu a networku
