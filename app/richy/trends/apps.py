from django.apps import AppConfig


class TrendsConfig(AppConfig):
    name = "richy.trends"
