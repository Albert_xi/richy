import logging
import time
from datetime import date

from karpet import Karpet

from ..celery import app
from .models import Trend

LOGGER = logging.getLogger(__name__)


@app.task
def fetch_trends_chart_data(trend_id=None):
    """
    Downloads trend charts data the given trend or for all
    trends in the database.
    """

    def fetch_google_trends_chart_data(trend):
        k = Karpet(trend.since, date.today())
        df = k.fetch_google_trends(kw_list=trend.keywords)

        trend.data = df.to_json()
        trend.save()

    def fetch_trend(trend):
        if Trend.GOOGLE == trend.network:
            fetch_google_trends_chart_data(trend)
            LOGGER.debug(
                f"Google trend chart data for item {trend.item.symbol} has been downloaded."
            )

        elif Trend.REDDIT == trend.network:
            raise NotImplementedError

    if trend_id:
        fetch_trend(Trend.objects.get(pk=trend_id))
    else:
        for trend in Trend.objects.all():
            try:
                fetch_trend(trend)
            except:
                LOGGER.exception(
                    "Couldn't download trends chart data.",
                    extra={"trend": trend.pk, "item": trend.item.symbol},
                )
                LOGGER.debug(f"Trend: {trend.pk} ^")

            time.sleep(10)
