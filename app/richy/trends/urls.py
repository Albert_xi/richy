from django.urls import path

from . import views

app_name = "trends"
urlpatterns = [
    path("update/<int:pk>/", views.TrendsFormView.as_view(), name="update"),
    path(
        "delete/<int:item_pk>/", views.TrendDeleteRedirectView.as_view(), name="delete"
    ),
]
