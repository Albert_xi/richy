import logging
import random
from datetime import date, timedelta
from unittest.mock import patch

import numpy as np
from django.urls import reverse
from django.utils import timezone

from ..core.math import calc_percentage_change
from ..core.models import Asset, ItemData, Price, UserItem
from ..core.scraper import CurrentPrice
from ..core.tests import (
    BaseCacheTestCase,
    BaseDecimalFormatTestCase,
    BaseDeleteUserItemTestCase,
    BaseItemOverviewAllocationTestCase,
    BaseTestCase,
    create_items,
    create_prices,
    create_transactions,
    create_user_items,
)
from ..transactions.models import Transaction
from .models import Dividend, Share
from .tasks import (
    fetch_basic_info,
    fetch_current_price,
    fetch_dividends,
    fetch_financial_data,
    fetch_historical_data,
    fetch_price_ratings,
    fetch_ratings,
)

LOGGER = logging.getLogger(__name__)


class HistoricalDataTestCase(BaseTestCase):
    def test_fetch_historical_data_without_transactions(self):
        """
        Tests fetching historical data (prices) of a share
        without any transactions in the database.
        """

        # Let's operate with TSLA ticket which should
        # have more than 1000 historical records.
        s = Share(symbol="TSLA")
        s.save()

        fetch_historical_data(s.pk)

        count = Price.objects.count()
        LOGGER.debug(f"Downloaded {count} historical records.")

        # Let's consider more than 1000 results as success.
        self.assertTrue(1000 < count)

    def test_fetch_historical_data_with_transactions(self):
        """
        Tests fetching historical data (prices) of a share
        with some transactions in the database.

        This test also indirectly tests following methods as long
        as they are called from ``fetch_historical_data()`` task.

        * update_caches()
        * generate_performance_charts()
        """

        # Let's operate with TSLA ticket which should
        # have more than 1000 historical records.
        s = Share(symbol="TSLA")
        s.save()

        # Also create a transaction.
        t = Transaction(
            item=s,
            price=250,  # pure guess
            amount=10,
            fee=10,
            date=date.today() - timedelta(days=300),
            is_deposit=True,
            user=self.user,
        )
        t.save()

        fetch_historical_data(s.pk)

        count = Price.objects.count()
        LOGGER.debug(f"Downloaded {count} historical records.")

        # Let's consider more than 1000 results as success.
        self.assertTrue(1000 < count)


class RatingsTestCase(BaseTestCase):
    def test_fetch_ratings(self):
        symbols = ["TSLA", "META", "MSFT", "QCOM", "ATVI", "BABA"]

        s = Share(symbol=random.choice(symbols))
        s.save()

        fetch_ratings(s)

        self.assertTrue(Asset.objects.filter(item=s, type=Asset.RATINGS_DATA).exists())


class PriceRatingsTestCase(BaseTestCase):
    def test_fetch_price_ratings(self):
        symbols = ["TSLA", "META", "MSFT", "QCOM", "BABA"]

        s = Share(symbol=random.choice(symbols))
        s.save()

        fetch_price_ratings(s)

        self.assertTrue(Asset.objects.filter(item=s, type=Asset.PRICE_RATINGS).exists())


class FinancialDataTestCase(BaseTestCase):
    def test_fetch_price_ratings(self):
        symbols = ["TSLA", "META", "MSFT", "QCOM", "BABA"]

        s = Share(symbol=random.choice(symbols))
        s.save()

        fetch_financial_data(s)

        self.assertTrue(Asset.objects.filter(item=s, type=Asset.EARNINGS_DATA).exists())
        self.assertTrue(Asset.objects.filter(item=s, type=Asset.REVENUES_DATA).exists())
        self.assertTrue(Asset.objects.filter(item=s, type=Asset.EPS_DATA).exists())


class PercentageChangesTestCase(BaseTestCase):
    def test_compounding_change(self):
        """
        Tests compounded change for AMD with 4 investments where
        each day price rose.
        """

        create_items()
        amd = Share.objects.get(symbol="AMD")
        create_transactions(
            [
                [
                    {
                        "item": amd,
                        "price": 10,
                        "amount": 1000,
                        "fee": 0,
                        "date": "2018-12-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": amd,
                        "price": 11,
                        "amount": 500,
                        "fee": 0,
                        "date": "2018-12-02",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": amd,
                        "price": 12,
                        "amount": 750,
                        "fee": 0,
                        "date": "2018-12-03",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": amd,
                        "price": 13,
                        "amount": 250,
                        "fee": 0,
                        "date": "2018-12-04",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ],
        )

        # Create price for item.
        Price.objects.create(item=amd, datetime="2018-12-01", price=10)
        Price.objects.create(item=amd, datetime="2018-12-02", price=11)
        Price.objects.create(item=amd, datetime="2018-12-03", price=12)
        Price.objects.create(item=amd, datetime="2018-12-04", price=13)
        Price.objects.create(item=amd, datetime="2018-12-05", price=14)
        Price.objects.create(item=amd, datetime="2018-12-06", price=15)

        # Calculate price change for last day.
        change = (
            Price.objects.order_by("-datetime")[0].price
            - Price.objects.order_by("-datetime")[1].price
        )

        # Calculate investment for each transactions.
        investments = []
        investment_perc_changes = []

        for trans in Transaction.objects.by_user(self.user).all():
            investment = trans.amount * trans.price
            investments.append(investment)
            investment_perc_changes.append(
                calc_percentage_change(
                    trans.amount * (trans.price + change), investment
                )
            )

        self.assertEqual(
            np.average(investment_perc_changes, weights=investments),
            amd.get_last_days_perc_change(1, no_cache=True, compounded=True),
        )


class BasicInfoTestCase(BaseCacheTestCase):
    def test_fetch_basic_info_all(self):
        create_items()
        fetch_basic_info()

        self.assertEqual(4, ItemData.objects.count())
        self.assertEqual(
            sorted(list(ItemData.objects.first().data["basic_info"].keys())),
            sorted(
                [
                    "company_name",
                    "market",
                    "description",
                    "market_cap",
                    "has_dividends",
                    "similar_stocks",
                    "yoy_change",
                    "year_low",
                    "year_high",
                    "pe_ratio",
                    "eps",
                ]
            ),
        )

    def test_fetch_basic_info(self):
        create_items()
        fetch_basic_info(Share.objects.first().pk)

        self.assertEqual(1, ItemData.objects.count())
        self.assertEqual(
            sorted(list(ItemData.objects.first().data["basic_info"].keys())),
            sorted(
                [
                    "company_name",
                    "market",
                    "description",
                    "market_cap",
                    "has_dividends",
                    "similar_stocks",
                    "yoy_change",
                    "year_low",
                    "year_high",
                    "pe_ratio",
                    "eps",
                ]
            ),
        )

    def test_detail_page_no_transactions(self):
        create_items()
        create_user_items()
        item = UserItem.objects.filter(item__share__isnull=False).first()
        fetch_basic_info(item.item.pk)

        response = self.c.get(reverse("shares:share_detail", args=[item.pk]))

        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Market cap", response.content)
        self.assertIn(b"Dividends", response.content)
        self.assertNotIn(b"Current wealth", response.content)

    def test_detail_page_one_transaction(self):
        create_items()
        create_user_items()
        item = (
            UserItem.objects.by_user(self.user)
            .filter(item__share__isnull=False)
            .first()
        )
        fetch_basic_info(item.item.pk)
        create_transactions(
            [
                [
                    {
                        "item": item.item,
                        "price": 10,
                        "amount": 1000,
                        "fee": 0,
                        "date": "2018-12-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ]
        )

        # Create price for item.
        Price.objects.create(item=item.item, datetime="2018-12-01", price=10)
        Price.objects.create(item=item.item, datetime="2018-12-02", price=11)

        response = self.c.get(reverse("shares:share_detail", args=[item.pk]))

        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Market cap", response.content)
        self.assertIn(b"Dividends", response.content)
        self.assertIn(b"Current wealth", response.content)
        self.assertIn(
            b'<span title="Investment">10,000</span>&nbsp; -> &nbsp;<span title="Market value">11,000</span>&nbsp;<span title="Amount">(1,000)</span>',
            response.content,
        )

    def test_detail_page_multiple_transactions(self):
        create_items()
        create_user_items()
        item = (
            UserItem.objects.by_user(self.user)
            .filter(item__share__isnull=False)
            .first()
        )
        fetch_basic_info(item.item.pk)
        create_transactions(
            [
                [
                    {
                        "item": item.item,
                        "price": 10,
                        "amount": 1000,
                        "fee": 0,
                        "date": "2018-12-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": item.item,
                        "price": 10,
                        "amount": 1000,
                        "fee": 0,
                        "date": "2018-12-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ]
        )

        # Create price for item.
        Price.objects.create(item=item.item, datetime="2018-12-01", price=10)
        Price.objects.create(item=item.item, datetime="2018-12-02", price=11)

        response = self.c.get(reverse("shares:share_detail", args=[item.pk]))

        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Market cap", response.content)
        self.assertIn(b"Dividends", response.content)
        self.assertIn(b"Current wealth", response.content)
        self.assertIn(
            b'<span title="Investment">20,000</span>&nbsp; -> &nbsp;<span title="Market value">22,000</span>&nbsp;<span title="Amount">(2,000)</span>',
            response.content,
        )


class DividendsTestCase(BaseTestCase):
    def test_dividends_all(self):
        create_items()
        fetch_dividends()

        self.assertGreaterEqual(Dividend.objects.count(), 60)

    def test_dividends(self):
        create_items()
        fetch_dividends(Share.objects.get(symbol="QCOM").pk)

        self.assertGreaterEqual(Dividend.objects.count(), 30)


class SplitterTestCase(BaseTestCase):
    reset_sequences = True

    def test(self):
        create_items()
        tsla = Share.objects.get(symbol="TSLA")
        create_prices(
            [
                ["2018-01-01", "TSLA", 100],
                ["2018-01-02", "TSLA", 110],
                ["2018-01-03", "TSLA", 120],
                ["2018-01-04", "TSLA", 130],
                ["2018-01-05", "TSLA", 140],
                ["2018-01-06", "TSLA", 150],
            ]
        )
        create_transactions(
            [
                [
                    {
                        "item": tsla,
                        "price": 110,
                        "amount": 1,
                        "fee": 0,
                        "date": "2018-01-02",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": tsla,
                        "price": 130,
                        "amount": 1,
                        "fee": 0,
                        "date": "2018-01-04",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ],
        )

        self.c.post(
            reverse("shares:splitter"),
            {"share": tsla.pk, "date": "2018-01-04", "ratio": 2},
        )
        self.c.post(reverse("shares:splitter_preview"))

        trans_1 = Transaction.objects.get(pk=1)
        trans_2 = Transaction.objects.get(pk=2)

        # First transaction is affected by the split.
        self.assertEqual(trans_1.amount, 2)
        self.assertEqual(trans_1.price, 55.0)

        # Second is left untact.
        self.assertEqual(trans_2.amount, 1)
        self.assertEqual(trans_2.price, 130)

        # Prices are empty (downloaded by celery which is turned off for test).
        self.assertFalse(Price.objects.filter(item=tsla).exists())


class CurrentPriceTestCase(BaseCacheTestCase):
    @patch("rug.yahoo.Yahoo.get_current_price_change")
    def test_share(self, mock):
        """
        Tests share current price, market state,  price change in
        percents and value caching.
        """

        # AMD data for 6.3.2022
        mock.return_value = {
            "state": "closed",
            "pre_market": {"change": {"percents": 0.0, "value": 0.0}, "value": 0.0},
            "current_market": {
                "change": {"percents": -3.1880688, "value": -3.5699997},
                "value": 108.41,
            },
            "post_market": {
                "change": {"percents": -0.14759123, "value": -0.16000366},
                "value": 108.25,
            },
        }
        create_items()

        share = Share.objects.get(symbol="AMD")
        fetch_current_price(share.pk)
        price = share.get_last_price(current=True)

        self.assertEqual(price.price, mock.return_value["post_market"]["value"])
        self.assertEqual(price.state, "closed")
        self.assertEqual(
            price.change_value, mock.return_value["post_market"]["change"]["value"]
        )
        self.assertEqual(
            price.change_percents,
            mock.return_value["post_market"]["change"]["percents"],
        )


class DeleteTestCase(BaseDeleteUserItemTestCase):
    model = Share
    url = "shares:delete_share"
    target_url = "shares:overview"
    symbol = "TSLA"
    task = fetch_historical_data

    def test(self):
        self.spawn()


class NewShareTestCase(BaseTestCase):
    def test_item_duplicity(self):
        create_items()
        create_user_items()

        response = self.c.post(
            reverse("shares:overview"),
            data={
                "symbol": Share.objects.first().symbol,
            },
        )
        self.assertEqual(
            {"symbol": ["This item has been already added."]},
            response.context["form"].errors,
        )


class OverviewAllocationViewTestCase(BaseItemOverviewAllocationTestCase):
    def test_allocation(self):
        self.prepare(
            [
                [str(timezone.now().date()), "AMD", 1],
                [str(timezone.now().date()), "QCOM", 2],
            ]
        )

        amd = Share.objects.get(symbol="AMD")
        qcom = Share.objects.get(symbol="QCOM")

        self.spawn(
            [
                [
                    {
                        "item": amd,
                        "price": 1,
                        "amount": 10,
                        "fee": 0,
                        "date": "2023-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": qcom,
                        "price": 2,
                        "amount": 10,
                        "fee": 0,
                        "date": "2023-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ],
            "shares:overview",
        )


class ShareDecimalFormatTestCase(BaseDecimalFormatTestCase):
    def test_price(self):
        self.perform_test_price_format(reverse("shares:overview"))

    def test_overview_chart_price(self):
        self.perform_test_overview_chart_price_format(reverse("shares:overview_fetch"))

    def test_item_detail_current_price(self):
        for user_item in self.user.useritem_set.filter(item__share__isnull=False):
            user_item.item.set_current_price_and_change(
                CurrentPrice(price=self.generate_price())
            )
            self.perform_test_item_detail_current_price_format(
                user_item.item.symbol,
                reverse("shares:share_detail", args=[user_item.pk]),
            )

    def test_item_detail_yoy_change(self):
        for user_item in self.user.useritem_set.filter(item__share__isnull=False):
            self.perform_test_item_detail_yoy_change(
                user_item.item,
                reverse("shares:share_detail", args=[user_item.pk]),
            )

    def test_item_detail_ath(self):
        for user_item in self.user.useritem_set.filter(item__share__isnull=False):
            self.perform_test_item_detail_ath(
                user_item.item, reverse("shares:share_detail", args=[user_item.pk])
            )

    def test_item_detail_year_low_high(self):
        for user_item in self.user.useritem_set.filter(item__share__isnull=False):
            self.perform_test_item_detail_year_low_high(
                user_item.item, reverse("shares:share_detail", args=[user_item.pk]), 2
            )

    def test_item_detail_chart_data(self):
        for user_item in self.user.useritem_set.filter(item__share__isnull=False):
            self.perform_test_item_detail_chart_price_format(
                user_item.item.symbol,
                reverse("shares:share_fetch", args=[user_item.pk]),
            )

    def test_item_performance_chart_data(self):
        for user_item in self.user.useritem_set.filter(item__share__isnull=False):
            self.perform_test_item_performance_chart_price_format(
                user_item.item.symbol,
                reverse("shares:performance", args=[user_item.pk]),
            )

    def test_item_performance_table_data(self):
        for user_item in self.user.useritem_set.filter(item__share__isnull=False):
            self.perform_test_item_performance_table_price_format(
                user_item.item.symbol,
                reverse("shares:performance", args=[user_item.pk]),
            )
