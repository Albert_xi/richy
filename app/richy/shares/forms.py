from django import forms
from django.utils.translation import gettext as _

from ..core.forms import BaseUserItemForm
from ..core.scraper import Manager
from .models import Share


class UserShareForm(BaseUserItemForm):
    item_model = Share

    def clean_symbol(self):
        symbol = super().clean_symbol()

        if not symbol:
            return symbol

        df = Manager.fetch_share_prices(Share(symbol=symbol), "1d")

        if df.empty:
            raise forms.ValidationError(_("Unknown symbol (according to YFinance)."))

        return symbol


class SplitterForm(forms.Form):
    share = forms.ModelChoiceField(queryset=Share.objects.all(), label=_("Share"))
    date = forms.DateField(label=_("Split date"))
    ratio = forms.FloatField(label=_("Ratio"))


class SplitterPreviewForm(forms.Form):
    pass
