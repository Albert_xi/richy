from django.contrib import admin

from ..core.admin import AssetInline, ItemDataInline, ItemMixin
from .models import Dividend, Share


class DividendsInline(admin.TabularInline):
    model = Dividend
    extra = 0


@admin.register(Share)
class ShareAdmin(ItemMixin, admin.ModelAdmin):
    inlines = [AssetInline, ItemDataInline, DividendsInline]
