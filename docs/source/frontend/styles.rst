Styles
======
App has tiny layer of own styles bacause it heavily utilizes material
components web framework. Although there are some slight overrides
to standard default which fits best to the app use case.

Overrides are done by direct "dirty" ``!important`` directive or by
mixins.
