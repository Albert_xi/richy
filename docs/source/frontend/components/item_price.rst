ItemPrice
=========
Fetches current market price, market state, move arrow
and percentage change once clicked. Serves for getting
fresh info from market for both shares and coins.

Component accepts following parameters:

- ``symbol`` - Symbol of the item
- ``defaultValue`` - Default/current value - market price
- ``moveId`` - ID of an element that holds market state, move
  arrow and percentage change
