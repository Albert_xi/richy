Assets
======
Assets are generated or downloaded assets which we save for
futher use. Model ``core.Asset`` handles all the data and each
row can be related to ``Item`` or to ``Transaction`` record.

Types of assets:

* ``EARNINGS_CHART`` - Earnings chart downloaded from YAHOO!.
* ``FINANCIALS_CHART`` - Financial chart downloaded from YAHOO!.
* ``TRENDS_CHART`` - Trends chart downloaded from YAHOO!.
* ``PERFORMANCE_CHART`` - Performance chart - generated.
* ``RECOMMENDATIONS_CHART`` - Recommendations chart - generated.
* ``TRENDS_TWITTER_CHART`` - Twitter trends chart.
* ``TRANSACTION_GRAPH`` - Transaction (dot) graph - generated.

Manual uploading
----------------
``Asset`` model has a convinient static method ``upload()`` which can be
used  for new asset creation. Method has following signature:

.. automethod:: richy.core.models.Asset.upload
