Blocks
======

The base template ``base.pug`` includes a few blocks that can be
utilized within other templates (like those included/rendered into
``content`` block).

- ``post_js`` - block reserved for short Javascript that is included on
  the very end of ``base.pug`` template
