Template tags
=============
App contains a few custom template tags for easier manipulation
with specific elements and values in templates.

Items
-----
Items library brings tags specific to items - shares, coins. Can be
loaded like:

.. code-block:: text

   - load items

.. automodule::  richy.core.templatetags.items
   :members:

Utils
-----
Utils bring various kinds of template tags no specifically related
to any topic. Can be loaded like:

.. code-block:: text

   - load utils

.. automodule::  richy.core.templatetags.utils
   :members:
