Richy's documentation
=====================

Architecture
------------
.. toctree::
   :maxdepth: 1

   architecture/basics
   architecture/cache
   architecture/dev
   architecture/frontend
   architecture/settings
   architecture/tasks
   architecture/templates/index

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Core
----
Application core stuff.

.. toctree::
   :maxdepth: 1

   core/assets
   core/charts
   core/fixtures
   core/form_fields
   core/math
   core/item
   core/scraper

Shares
------
Module with all functionality realted to stock shares.

.. toctree::
   :maxdepth: 1

   shares/charts
   shares/tasks

Indexes
-------
Module with all functionality ralated to indexes.

.. toctree::
   :maxdepth: 1

   indexes/tasks

Coins
-----
Module with all functionality realted to cryptocurrencies (coins).

.. toctree::
   :maxdepth: 1

   coins/coin_ids
   coins/tasks

Transactions
------------
Module with transactions and related charts.

.. toctree::
   :maxdepth: 1

   transactions/charts
   transactions/dividends
   transactions/performance
   transactions/tasks
   transactions/transactions

News
----
Module fetching news from the internet.

.. toctree::
   :maxdepth: 1

   news/index

Staking
-------
Application for stakings with interconnection to transaction :doc:`./transactions/charts`.

Trends
------
Module that handles all trends scraping.

.. toctree::
   :maxdepth: 1

   trends/tasks

Frontend
--------
All frontend stuff - components, styles, ...

.. toctree::
   :maxdepth: 1

   frontend/components/index
   frontend/styles

Server
------
Server stuff

.. toctree::
   :maxdepth: 1

   server/basics
   server/certificates
   server/ci
