Tasks
=====

fetch_trends_chart_data(trend_id=None)
--------------------------------------
Fetches trend charts data for all kinds off trends. Task can be called
with ``trend_id`` or without. If no trend is given task downloads
chart data for all trends in the database with 10 seconds pause.
