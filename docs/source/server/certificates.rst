Server
======

Certificates
------------
Project uses `Certbot <https://certbot.eff.org/>`_ for certificates. Certbot
provides docker image that can be used separetely from our app to obtain
the certtificate.

The idea is to run certbot on the server where it spins up temporary server
and obtains the certificate. The trick is to have volume for ``/etc/letsencrypt``
directory. It results in saving all certificate stuff directly onto server.
We can than run our app container and mount this directory as volume. Nginx
packed with our image will have access to the certificate.

.. note::

   When generating/renewing certificate the app container must be off - down.
   It blocks port 80 so certbot container wouldn't be able to kick off
   temporary server.

To run certbot on the server run following:

::

   docker run -it --rm -p 80:80 -v /etc/richy_letsencrypt:/etc/letsencrypt certbot/certbot certonly

and reply to the certbot script questions like:

::

   1: 1
   2. enter email
   3. Agree
   4. No
   5. enter app domain

Now all certificate stuff are saved directly on the server (not in the certbot container) in
``/etc/richy_letsencrypt`` directory.

Now kick off the app (just start supervisor task like ``supervisorctl start richy``).

.. Renewal
.. ~~~~~~~
.. Once the certificate is about to expire it's necessary to renew it. Whole
.. process can be done via `certbot` command like:

.. ::

..    certbot renew

.. Then again whole folder `/etc/letsencrypt` should me backed up.

.. .. note:: Each command must be run under root user.
